const form = document.querySelector('.password-form');
const btn = document.querySelector('.btn');
const firstPassword = document.querySelector('#firstPassword');
const secondPassword = document.querySelector('#secondPassword');
const textError = document.querySelector('p');

function makeAnswer() {
    if (textError.classList.contains('error')) {
        textError.classList.remove('error');
    };
    alert('You are welcome');
};

function changeVisibleInput(icon) {
    const field = document.querySelector(`#${icon.getAttribute('id')}Password`);
    if (icon.classList.contains('fa-eye-slash')) {
        field.setAttribute('type', 'password');
    } else {
        field.setAttribute('type', 'text');
    };
};

form.addEventListener('click', function (event) {
    if (event.target.tagName === 'I') {
        const icon = event.target;
        icon.classList.toggle('fa-eye-slash');
        icon.classList.toggle('fa-eye');
        changeVisibleInput(icon);
    };
});

btn.addEventListener('click', function (event) {
    event.preventDefault();
    if (firstPassword.value === secondPassword.value) {
        makeAnswer();
    } else if (firstPassword.value != secondPassword.value) {
        textError.classList.add('error');
    };
});